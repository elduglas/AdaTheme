# Tema GNOME >= 40

 - Descargar/Clonar el repositorio.
 - Crear la carpeta .themes dentro de la raíz del home.
 - Copiar la carpeta clonada en la carpeta .themes.
 - Dentro de la carpeta clonada se encuentra una carpeta llamada gtk4.0, copiar dicho contenido en la carpeta .config/gtk-4.0.
 - Para cambiar el tema se requiere la aplicación retoques(tweaks) de GNOME.
 - Para que los cambios en las aplicaciones de GNOME tengan efecto se debe cerrar la sesión y volver a iniciar.
![enter image description here](https://gitlab.com/elduglas/AdaTheme/-/raw/master/AdaTheme.png?ref_type=heads)
